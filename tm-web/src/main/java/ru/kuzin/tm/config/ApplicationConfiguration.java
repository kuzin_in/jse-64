package ru.kuzin.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.kuzin.tm")
public class ApplicationConfiguration {
}