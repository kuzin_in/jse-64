package ru.kuzin.tm.repository;

import org.springframework.stereotype.Repository;
import ru.kuzin.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

    private ProjectRepository() {
    }

    {
        add(new Project("Project 1"));
        add(new Project("Project 2"));
        add(new Project("Project 3"));
    }

    public void create() {
        add(new Project("New Project " + System.currentTimeMillis()));
    }

    public void add(Project project) {
        projects.put(project.getId(), project);
    }

    public void save(Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(String id) {
        return projects.get(id);
    }

    public void removeById(String id) {
        projects.remove(id);
    }

}